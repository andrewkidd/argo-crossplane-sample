# ArgoCD / Crossplane demo
##### _GitOps with Kubernetes/Crossplane/ArgoCD in an Azure Subscription_

## About
The repo should contain everything needed to create a test/demo Kubernetes Cluster running Crossplane and ArgoCD with some demo applications.

ArgoCD is configured to watch the repo for changes to the applications, with auto-sync and auto-heal enabled. When changes are made to the application definitions in Git, ArgoCD will attempt to modify the deployed resources to match the source definition, no pipeline necessary.

## Usage
This repo is [built to be forked](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html). There is some [initial setup](Components/GitLab.md#setup) required to allow the project to execute properly.
The repo CD pipeline should then automate all deployment, installation and configuration of resources and applications and documentation.

## Project Components
This project is made possible by utlizing the following components:

1. [GitLab Pipelines](Components/GitLab.md)
   1. [Terraform](Components/Terraform.md)
2. [Kubernetes](Components/Kubernetes.md)
   1. [Crossplane](Components/Crossplane.md)
       1. [Crossplane Azure Provider](Components/Crossplane.md#azure-provider)
   2. [ArgoCD](Components/ArgoCD.md)
       1. [Declarative ArgoCD Application Manifests](Components/ArgoCD.md#applications) 

## Sources / Resources
##### Docs
- [Crossplane Docs](https://docs.crossplane.io/) 
- [Crossplane Azure Provider](https://marketplace.upbound.io/providers/upbound/provider-azure/)
- [Argo CD Docs](https://argo-cd.readthedocs.io/)
##### Guides
- [TechTarget - Step-by-step guide to working with Crossplane and Kubernetes](https://www.techtarget.com/searchitoperations/tutorial/Step-by-step-guide-to-working-with-Crossplane-and-Kubernetes) 
- [Crossplane - Azure Quickstart](https://docs.crossplane.io/v1.11/getting-started/provider-azure/)
##### Videos
- [CNCF - Crossplane Intro & Deep Dive](https://www.youtube.com/watch?v=xECc7XlD5kY)
- [CNCF - ArgoCD and Crossplane for Infrastructure Management](https://www.youtube.com/watch?v=QO_-PG9snvI)
- [TechWorld with Nana - ArgoCD Tutorial for Beginners](https://www.youtube.com/watch?v=p-kAqxuJNik)
- [DevOps ToolKit - How To Apply GitOps to Everything](https://www.youtube.com/watch?v=yrj4lmScKHQ&t=967s)