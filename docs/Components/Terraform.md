# Terraform
[Terraform is an open-source infrastructure as code software tool that enables you to safely and predictably create, change, and improve infrastructure.](https://www.terraform.io/)

## Implementation
#### Usage
Terraform is leveraged via the pipeline 'deploy' stage, in order to create a [Kubernetes Cluster](./Kubernetes.md) on Azure
```
    - sh cd/scripts/tasks.sh terraform-init
    - sh cd/scripts/tasks.sh terraform-plan
    - sh cd/scripts/tasks.sh terraform-apply
    - sh cd/scripts/tasks.sh terraform-output-json
```

In this project the terraform config is stored under /cd/terraform