# GitLab

## About
This project uses GitLab to:

 - Store the Repository itself
 - Deploy to Azure via GitLab CI/CD Piplines
 - Documentation Deployment + Hosting

In this project the pipeline definitions are stored as YAML files under /cd/gitlab/

## Setup
###### GitLab Pipeline Requirements

1.  Create a Service Principal in Azure to allow automated login to your subscription(s). This can be done either:
	1.  Via the [Azure Portal](https://portal.azure.com/#view/Microsoft_AAD_RegisteredApps/ApplicationsListBlade)
    	1.  create sp with name 'deploy-service-principal'
    	2.  add the sp as application administrator
	2.  Via [Az CLI](https://learn.microsoft.com/en-us/cli/azure/install-azure-cli)
		1.  az login
		2.  az ad sp create-for-rbac --sdk-auth --role Owner -n deploy-service-principal
		3.  add the sp as application administrator
2.  Set the following under 'Project > Settings > CI/CD > Variables'
	- ARM_CLIENT_ID: `Service Principal Application ID`
	- ARM_CLIENT_SECRET: `Service Principal Client Secret`
	- ARM_SUBSCRIPTION_ID: `Azure Subscription ID`
	- ARM_TENANT_ID: `Azure Tenant ID`
3. There is a [special deploy token](https://docs.gitlab.com/ee/user/project/deploy_tokens/#gitlab-deploy-token) that is automatically exposed to CI/CD pipelines and gives access to the GitLab projects resources, in this case the Container Registry.  
    Create this token under 'Project > Settings > Repository > Deploy Tokens'
	- Name: gitlab-deploy-token
	- Scopes: 
		- read_repository

###### Running Pipelines
With the above requirements in place, a push to the master branch should kick off a successful deployment.

   - Monitor the pipeline progress under 'CI/CD > Pipelines'
   - Navigate to [Azure Portal > Kubernetes Services](https://portal.azure.com/#blade/HubsExtension/BrowseResource/resourceType/Microsoft.ContainerService%2FmanagedClusters) to verify results