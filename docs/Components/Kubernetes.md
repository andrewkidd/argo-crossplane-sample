# Kubernetes
[Kubernetes, also known as K8s, is an open-source system for automating deployment, scaling, and management of containerized applications.](https://kubernetes.io/)

## Implementation

In this project, Kubernetes is provisioned on Azure as a [Managed Kubernetes Service](https://azure.microsoft.com/en-gb/products/kubernetes-service)

#### Installation
Kubernetes is initialized via the pipeline 'deploy' stage. The definition is written in [Terraform/HCL](./Terraform.md) and the state is managed via GitLab.

The cluster resource group and name are printed to the log, you can use these to connect to the cluster with AZ CLI and Kubectl