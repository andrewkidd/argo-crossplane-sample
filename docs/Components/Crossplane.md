# Crossplane
[Crossplane is a framework for building cloud native control planes without needing to write code.](https://www.crossplane.io/)

## Implementation
#### Installation
Crossplane runs inside the [Kubernetes](./Kubernetes.md) cluster itself, so the cluster must exist first. To solve this the initial creation of the cluster is handled via [Terraform](./Terraform.md).

Crossplane is then installed via the pipeline 'install' stage. A namespace 'crossplane-system' is created, and then crossplane is installed to it via Helm as per the '[Install Crossplane](https://docs.crossplane.io/v1.11/software/install/)' section on the Crossplane docs.
```
	- kubectl config current-context
	- kubectl create namespace crossplane-system --dry-run=client -o yaml | kubectl apply -f -
	- helm repo add crossplane-stable https://charts.crossplane.io/stable
	- helm upgrade --install crossplane --namespace crossplane-system crossplane-stable/crossplane
	- kubectl wait deployment -n crossplane-system crossplane --for condition=Available=True
```

#### Configuration

Once Crossplane is installed the pipeline 'configure' stage installs some '[Providers](https://docs.crossplane.io/v1.11/concepts/providers/)' to the namespace. Providers define the **C**ustom **R**esource **D**efinitions that allow Crossplane to manage infrastructure.

In this project we're installing Providers that require Azure Authentication, so firstly the pipeline will create an Azure Service Principal and store it as a secret in the cluster
```
    - sh cd/scripts/tasks.sh az-ad-sp-create-for-rbac
    - sh cd/scripts/tasks.sh az-ad-sp-permissions
    - kubectl create secret generic azure-secret -n crossplane-system --from-file=creds=cd/kubernetes/crossplane-azure-provider-key.json --dry-run=client -o yaml | kubectl apply -f - 
```

The provider configuration manifest files for these providers reference the secret above, and use it for authentication. The config stage applies the provider manifest, waits for it to be ready and then applies the provider config manifest.

```
    - kubectl apply -f cd/kubernetes/manifests/az-provider.yml -n crossplane-system
    - kubectl wait provider -n crossplane-system upbound-provider-azure --for condition=Healthy=True
    - kubectl apply -f cd/kubernetes/manifests/az-providerconfig.yml -n crossplane-system

```

### Usage
Crossplane runs as a [control plane](https://kubernetes.io/docs/concepts/overview/components/#control-plane-components) in the cluster. Crossplane detects when resources are requested that fall under it's providers and responds appropriately.

## Azure Provider
The [Upbound Azure provider](https://marketplace.upbound.io/providers/upbound/provider-azure/v0.28.0) allows direct resource management on Azure.

## Terraform Provider
The [Upbound Terraform provider](https://marketplace.upbound.io/providers/upbound/provider-terraform/v0.5.0) allows you to leverage existing HCL and Terraform Providers to manage resources