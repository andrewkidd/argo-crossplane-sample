# ArgoCD
[Argo CD is a declarative, GitOps continuous delivery tool for Kubernetes.](https://argo-cd.readthedocs.io/en/stable/)

## Implementation
#### Installation
ArgoCD runs inside the [Kubernetes](./Kubernetes.md) cluster itself, so the cluster must exist first. To solve this the initial creation of the cluster is handled via [Terraform](./Terraform.md).

ArgoCD is then installed via the pipeline 'install' stage. The manifest is applied to the cluster via kubectl into it's own namespace, as per the '[Getting Started](https://argo-cd.readthedocs.io/en/stable/getting_started/)' section on ArgoCD docs.
```
	- kubectl config current-context
    - kubectl create namespace argocd --dry-run=client -o yaml | kubectl apply -f -
    - kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
    - >
      kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "LoadBalancer"}}'
    - kubectl wait deployment -n argocd argocd-server --for condition=Available=True
```

**NB: currently I'm patching argocd to change the spec type to LoadBalancer as I don't have a proper ingress defined at the moment.**

#### Configuration

Once ArgoCD is running the pipeline 'configure' stage queries the cluster for the provisioned IP Address and Password and authenticates against it using the ArgoCD CLI.

After authentication, the pipeline adds the current repository to the ArgoCD settings, which allows it to access the repo unattended.

Finally, the [ArgoCD application specs](https://argo-cd.readthedocs.io/en/stable/operator-manual/declarative-setup/#applications) are applied to the cluster. 
These specs are stored in a single manifest file for simplicity, and contain the configuration for every app that should be managed by ArgoCD. It's the application spec that tells ArgoCD which repository, branch and path to monitor for changes.

The ArgoCD IP, username and password is then printed in the log, you can log in to the WebUI with these details.

In this project the app specs are stored under /cd/kubernetes/argocd/applications.yml

## Applications
#### Definitions
ArgoCD monitors defined paths in a repository. If it detects changes in either the source or the deployed resources, it runs a 'Sync' to attempt to bring the deployed resources back into alignment with the source.

In this project the application definitions are stored under /apps/. We currently have:

 - **azure-app**  
	An app that deploys some infrastructure to Azure via the [Upbound Azure Provider](./Crossplane.md#azure-provider)
 - **[kustomize-guestbook](https://github.com/argoproj/argocd-example-apps/tree/master/kustomize-guestbook)**  
	An app that deploys to the cluster itself

Note that while ArgoCD applies the manifests to the cluster, it is up to the cluster control plane to action the changes.