output "kubernetes_service_client_certificate" {
    value = azurerm_kubernetes_cluster.cluster_kubernetes_service_nodes.kube_config.0.client_certificate
    sensitive = true
}

output "kubernetes_service_kube_config" {
    value = azurerm_kubernetes_cluster.cluster_kubernetes_service_nodes.kube_config_raw
    sensitive = true
}