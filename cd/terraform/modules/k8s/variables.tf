variable "cluster_name" {
  type = string
}

variable "cluster_sku" {
	type = string
}

variable "cluster_resource_group_name" {
  type = string
}

variable "cluster_location" {
  type = string
}

variable "cluster_default_pool_node_count" {
  type = number
}

variable "cluster_default_pool_vm_size" {
  type = string
}