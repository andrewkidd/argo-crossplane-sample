resource "azurerm_kubernetes_cluster" "cluster_kubernetes_service_nodes" {
  name                = var.cluster_name
  sku_tier			  = var.cluster_sku
  location            = var.cluster_location
  resource_group_name = var.cluster_resource_group_name
  dns_prefix          = var.cluster_name

  key_vault_secrets_provider {
    secret_rotation_enabled = true
  }

  default_node_pool {
    name       = "agentpool"
    node_count = var.cluster_default_pool_node_count
    vm_size    = var.cluster_default_pool_vm_size
  }

  identity {
    type = "SystemAssigned"
  }
}