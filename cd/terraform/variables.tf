variable "cluster_name" {
  type = string
  default = "argo-crossplane-sample"
  description = "Base name used for cluster related resources"
}

variable "cluster_sku" {
  type = string
  default = "Paid"
  description = "The SKU Tier that should be used for this Kubernetes Cluster. Possible values are Free and Paid"
}

variable "cluster_location" {
  type = string
  default = "uksouth"
  description = "Primary location for cluster service to be deployed to"
}

variable "cluster_default_pool_node_count" {
  type = number
  default = 1
  description = "Number of nodes to deploy in clusters default pool"
}

variable "cluster_default_pool_vm_size" {
  type = string
  default = "Standard_B4ms"
  description = "Default VM size of cluster nodes"
}