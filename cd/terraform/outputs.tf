output "kubernetes_service_resource_group_name" {
    value = local.resource_group_name
    sensitive = false
}

output "kubernetes_service_name" {
    value = local.cluster_name
    sensitive = false
}

output "kubernetes_service_client_certificate" {
    value = module.kubernetes_service.kubernetes_service_client_certificate
    sensitive = true
}

output "kubernetes_service_kube_config" {
    value = module.kubernetes_service.kubernetes_service_kube_config
    sensitive = true
}