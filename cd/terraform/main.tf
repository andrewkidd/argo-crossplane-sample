# We strongly recommend using the required_providers block to set the
# Azure Provider source and version being used
terraform {
  backend "http" {}
  required_providers {
    azurerm	= {
      source	= "hashicorp/azurerm"
      version	= "=3.45.0"
    }
  }
}

# Configure the Microsoft Azure Provider
provider "azurerm" {
  features {}
}

# Configure any local variables
locals {
    cluster_name			= "${var.cluster_name}-k8s"
	resource_group_name		= "${var.cluster_name}-resource-group"
}

# Prepare app resource group
resource "azurerm_resource_group" "cluster_resource_group" {
	name		= local.resource_group_name
	location	= var.cluster_location
}

# Load in kubernetes deploy module
module "kubernetes_service" {
	source							= "./modules/k8s"
	cluster_name					= local.cluster_name
	cluster_sku						= var.cluster_sku
	cluster_resource_group_name		= azurerm_resource_group.cluster_resource_group.name
	cluster_location				= azurerm_resource_group.cluster_resource_group.location
	cluster_default_pool_node_count	= var.cluster_default_pool_node_count
	cluster_default_pool_vm_size	= var.cluster_default_pool_vm_size 
}